// handles the posts
let posts = [];

// id count
let count = 1;

// Add post

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	// this save the post
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	count++;

	showPosts(posts);
	alert("Post successfully added");

	console.log(posts);
	console.log(count);
});

const showPosts = (posts) => {
	// This will take the new post
	let postEntries = '';

	
	// for each post created, create a new post to be shown
	posts.forEach( (post) => {

		postEntries += `
			<div id="posts-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
			
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>

			</div>
		`;

		// console.log(postEntries);
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// Edit post function

// id came from the button onclick editPost ${post.id} this will pass a value to the function
const editPost = (id) => {


	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// Check value of the selected id
	console.log("txt-edit-ID: " + String(id));
};


// Update function
document.querySelector("#form-edit-post").addEventListener("submit", (e)=> {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){

			posts[i].title = document.querySelector('#txt-edit-title').value;

			posts[i].body = document.querySelector('#txt-edit-body').value;

			console.log(posts.length);
			console.log(document.querySelector("#txt-edit-id").value);
			console.log(`POST ID TO EDIT: ${i} + 1`);
		}

		showPosts(posts);
		// alert("Post successfully updated");



		// break;
	}
});



// Delete post function
const deletePost = (id) => {
	
	// let removePost = document.querySelector(`posts-${id}`);
	// removePost.remove();


	// let title = document.querySelector(`#post-title-${id}`).innerHTML;
	// let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// document.querySelector(`posts-${id}`).value = '';
	// document.querySelector(`post-title-${id}`).value = '';
	// document.querySelector(`post-body-${id}`).value = '';





	console.log(id.toString());

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === id)
		{
			posts.splice(i, 1);

			document.querySelector(`#posts-${id}`).innerHTML = '';


			//remove whole
			/*
			<div id="posts-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
			
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>

			</div>
			*/
			// let removePost = document.querySelector(`posts-${id}`);
			// removePost.parentNode.removeChild(removePost);
			

			console.log("Array remove has id of: " + id);

			console.log(posts);
		}
	}
}






// const editPost = (id) => {


// 	let title = document.querySelector(`#post-title-${id}`).innerHTML;
// 	let body = document.querySelector(`#post-body-${id}`).innerHTML;

// 	document.querySelector("#txt-edit-id").value = id;
// 	document.querySelector("#txt-edit-title").value = title;
// 	document.querySelector("#txt-edit-body").value = body;

// 	// Check value of the selected id
// 	console.log("txt-edit-ID: " + String(id));
// };